FROM timbru31/java-node:latest
COPY ./build/libs/deliveryapplication-0.1.0.jar deliveryapplication-0.1.0.jar
COPY ./build/resources/main/application.properties application.properties
CMD ["java","-jar","deliveryapplication-0.1.0.jar"]