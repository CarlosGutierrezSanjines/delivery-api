package com.devops.delivapp.model;

import java.util.Objects;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Size;

@Entity
@Table(name="products")
public class Product {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Integer id;

  @Size(min = 5, max = 50)
  private String p_name;
  private int price;

  public Product(){

  }

  public Product(String name, int price){
    this.p_name = name;
    this.price = price;
  }

  /**
   * @return the id
   */
  public Integer getId() {
    return id;
  }
  /**
   * @return the p_name
   */
  public String getP_name() {
    return p_name;
  }
  /**
   * @return the price
   */
  public int getPrice() {
    return price;
  }
  /**
   * @param id the id to set
   */
  public void setId(Integer id) {
    this.id = id;
  }
  /**
   * @param p_name the p_name to set
   */
  public void setP_name(String p_name) {
    this.p_name = p_name;
  }
  /**
   * @param price the price to set
   */
  public void setPrice(int price) {
    this.price = price;
  }
}