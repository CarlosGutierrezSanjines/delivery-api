package com.devops.delivapp.repository;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;

import com.devops.delivapp.model.Product;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.junit4.SpringRunner;


@RunWith(SpringJUnit4ClassRunner.class)
@DataJpaTest
public class ProductsRepositoryUnitTest {

  @Autowired
  private IProductsRepository repository;

  @Test
  public void whenFindFirst_ComparePrices() {

    Product oleole = new Product("oleole", 3);
    repository.save(oleole);

    Product coca = new Product("verde", 10);
    repository.save(coca);

    List<Product> found = repository.findAll();
    System.out.println(found.get(0));
    assertEquals(oleole.getPrice(), found.get(0).getPrice(), "Found produt must have the same price");
  }

}